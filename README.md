# Applications of mathematics in computer science

Course material

## Lecture subjects

1. Course organization, online resources, logarithm and exponent.
1. Taylor series and approximate function computation.
1. Modular arithmetics, hashing, and cryptography.
1. Eigenvalues and eigenvectors, Google PageRank algorithm.
1. Differentiation, optimization,  automatic differentiation.
1. Complex numbers, Fourier transform, signal processing, 
   error correction.
